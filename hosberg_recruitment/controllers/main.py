# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http
from odoo.http import request
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.addons.survey.controllers.main import WebsiteSurvey
from odoo.addons.website_hr_recruitment.controllers.main import \
    WebsiteHrRecruitment


class CrmWebsiteForm(WebsiteForm):
    # Check and insert values from the form on the model <model>
    @http.route('/website_form/<string:model_name>',
                type='http', auth="public", methods=['POST'], website=True)
    def website_form(self, model_name, **kwargs):
        if model_name == 'hr.applicant':
            geoip_country_code = \
                request.session.get('geoip', {}).get('country_code')
            geoip_state_code = request.session.get('geoip', {}).get('region')
            if geoip_country_code and geoip_state_code:
                State = request.env['res.country.state']
                request.params['state_id'] = State.search(
                    [('code', '=', geoip_state_code),
                     ('country_id.code', '=', geoip_country_code)]).id

        return super(CrmWebsiteForm, self).website_form(model_name, **kwargs)


class HosbergWebsiteSurvey(WebsiteSurvey):
    @http.route([],
                type='http', auth='public', website=True)
    def print_survey(self, survey, token=None, **post):
        value = {'survey': survey,
                 'token': token,
                 'page_nr': 0,
                 'user_input_id': post.get('user_input_id'),
                 'quizz_correction': True if survey.quizz_mode
                                             and token else False}
        return request.render('survey.survey_print', value)


class HosbergWebsiteHrRecruitment(WebsiteHrRecruitment):
    @http.route([], type='http', auth="public",
                website=True)
    def jobs_apply(self, job, **kwargs):
        error = {}
        default = {}
        titles = request.env['res.partner.title'].search([])
        if 'website_hr_recruitment_error' in request.session:
            error = request.session.pop('website_hr_recruitment_error')
            default = request.session.pop('website_hr_recruitment_default')
        return request.render("website_hr_recruitment.apply", {
            'job': job,
            'titles': titles,
            'error': error,
            'default': default,
        })
