# -*- coding: utf-8 -*-

##############################################################################
#
#    Clear Groups for Odoo
#    Copyright (C) 2016 Bytebrand GmbH (<http://www.bytebrand.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Hosberg Recruitment',
    'sequence': 120,
    'depends': [
        'hr',
        'hr_recruitment',
        'website_form',
        'hr_recruitment_survey',
        'survey',
    ],
    'version': '11.0.0.0.1',
    'category': 'Human Resources',
    'summary': 'Recruitment',
    'description': """
    """,
    'data': [
        'security/survey_security.xml',
        'security/ir.model.access.csv',

        'data/config_data.xml',
        'data/hosberg_email_templates.xml',

        'views/hr_recruitment_views.xml',
        'views/website_hr_recruitment_templates.xml',
        'views/mail_template_views.xml',
        'views/survey_views.xml',

        'views/survey_templates.xml',

        'views/report.xml',
        'data/new_survey_completed.xml',

        'views/source.xml',

        'wizard/confirm_applicant_stage_views.xml',

        # 'security/security_data.xml',
    ],

    'images': ['static/description/icon.png'],
    'installable': True,
    'auto_install': False,
    'application': True,

    'js': [],
    'qweb': [],
}
