# -*- coding: utf-8 -*-

##############################################################################
#
#    Clear Groups for Odoo
#    Copyright (C) 2016 Bytebrand GmbH (<http://www.bytebrand.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
import base64
from odoo import api, fields, models, SUPERUSER_ID, _

_logger = logging.getLogger(__name__)


class Survey(models.Model):
    _inherit = 'survey.survey'

    recruitment_survey = fields.Boolean(string='Recruitment Survey')
    responsible_employee_id = fields.Many2one('hr.employee',
                                               string="Responsible Employee")

    @api.multi
    def action_print_survey(self):
        self.ensure_one()
        token = self.env.context.get('survey_token')
        if self.env.context.get('user_input_id'):
            trail = '/{}?user_input_id={}'.format(token if token else "",
                                                  self.env.context.get(
                                                      'user_input_id').id)
        else:
            trail = "/" + token if token else ""
        return {
            'type': 'ir.actions.act_url',
            'name': "Print Survey",
            'target': 'self',
            'context': self.env.context,
            'url': self.with_context(relative_url=True).print_url + trail
        }

    @api.multi
    def get_answer(self, question, user_input_id):
        if user_input_id:
            answer = self.env['survey.user_input_line'].search(
                [('question_id', '=', question.id),
                 ('user_input_id', '=', int(user_input_id))])
            return answer

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        result = []
        res = super(Survey, self).read(fields=fields, load=load)
        if self.env.uid == 1 or self.env.user.has_group(
                'hr_recruitment.group_hr_recruitment_user'):
            return res
        else:
            for r in res:
                if not r.get('recruitment_survey'):
                    result.append(r)
        return result


class SurveyUserInput(models.Model):
    _inherit = "survey.user_input"

    applicant_id = fields.Many2one('hr.applicant',
                                   string="Applicant")

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        res = super(SurveyUserInput, self).read(fields=fields, load=load)
        recruitment_survey_ids = self.env['survey.survey'].search(
            [('recruitment_survey', '=', True)]).ids
        result = []
        for r in res:
            if r.get('survey_id'):
                if type(r.get('survey_id')) == int:

                    survey_id = r.get('survey_id')
                else:
                    survey_id = r.get('survey_id')[0]

                if survey_id not in recruitment_survey_ids:
                    result.append(r)
        return result

    @api.multi
    def write(self, values):
        res = super(SurveyUserInput, self).write(values)
        if self.state == 'done':
            template_id = self.env['ir.model.data'].get_object_reference(
                'hosberg_recruitment', 'email_template_new_survey')[1]
            if template_id:
                mail_template = self.env['mail.template'].browse(
                    template_id)
                if mail_template:
                    mail_template.sudo().send_mail(
                        res_id=self.id, force_send=True)
        return res


class ApplicantSurveys(models.Model):
    _name = "applicant.surveys"

    applicant_id = fields.Many2one('hr.applicant',
                                   string="Applicant")
    survey_id = fields.Many2one('survey.survey',
                                string="Applicant")
    user_input_id = fields.Many2one('survey.user_input',
                                    string="Answer")
    state = fields.Selection([('new', 'Not started yet'),
                              ('skip', 'Partially completed'),
                              ('done', 'Completed')],
                             string='Status',
                             default='new',
                             readonly=True,
                             related='user_input_id.state')

    @api.multi
    def write(self, values):
        return super(ApplicantSurveys, self).write(values)

    @api.model
    def create(self, values):
        applic_id = self.env.context.get('active_id')
        if applic_id:
            values.update({'applicant_id': applic_id})
        res = super(ApplicantSurveys, self).create(values)
        user_input_id = self.env['survey.user_input'].create(
            {'survey_id': res.survey_id.id,
             'applicant_id': res.applicant_id.id})
        res.user_input_id = user_input_id.id
        return res

    @api.multi
    def action_start_survey(self):
        self.ensure_one()
        if not self.user_input_id:
            user_input_id = self.env['survey.user_input'].create(
                {'survey_id': self.survey_id.id,
                 'applicant_id': self.applicant_id.id})
            self.user_input_id = user_input_id.id
        else:
            user_input_id = self.user_input_id
        return self.survey_id.with_context(
            survey_token=user_input_id.token).action_start_survey()

    @api.multi
    def action_print_survey(self):
        self.ensure_one()

        if not self.user_input_id:
            return self.survey_id.action_print_survey()
        else:
            user_input_id = self.user_input_id
            return self.survey_id.with_context(
                survey_token=user_input_id.token,
                user_input_id=user_input_id).action_print_survey()


class SurveyQuestion(models.Model):
    _inherit = 'survey.question'

    type = fields.Selection(selection_add=[
        ('binary', 'Upload File')])

    @api.multi
    def validate_binary(self, post, answer_tag):
        errors = {}
        answer = post[answer_tag]
        if self.constr_mandatory and not answer:
            errors.update({answer_tag: self.constr_error_msg})
        return errors


class UserInput(models.Model):
    _inherit = 'survey.user_input_line'

    @api.multi
    def _compute_survey_answer_file(self):
        base_url = '/' if self.env.context.get('relative_url') else self.env[
            'ir.config_parameter'].sudo().get_param('web.base.url')
        for surv in self:
            surv.url_pdf_download_file = base_url + '/web/content/' + str(surv.attachment_id.id)

    @api.multi
    def download_attachment(self):
        a = {'name': 'Attachment',
             'type': 'ir.actions.act_url',
             'url': '/web/content/{}'.format(self.attachment_id.id),
             'target': 'new'}
        return a

    answer_type = fields.Selection(selection_add=[('binary', 'Upload File')])
    attachment_id = fields.Many2one('ir.attachment', string='Attachment')
    url_pdf_download_file = fields.Char("base_url",
                                        compute="_compute_survey_answer_file")

    @api.model
    def save_line_binary(self, user_input_id, question, post, answer_tag):
        # print('\n Зашел в save_line_binary >>>>>>')
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'page_id': question.page_id.id,
            'survey_id': question.survey_id.id,
            'skipped': False
        }
        attachment_value = {}
        record = self.search([
            ('user_input_id', '=', user_input_id),
            ('survey_id', '=', question.survey_id.id),
            ('question_id', '=', question.id)], limit=1)

        attachment = self.env['ir.attachment'].sudo(SUPERUSER_ID)
        # print('save_line_binary - post>>>>>>>>', post)
        # print('save_line_binary - answer_tag>>>>>>>>', post.get(answer_tag))
        if post.get(answer_tag, '') != '':
            file = post[answer_tag]
            attachment_value = {
                'name': file.filename,
                'datas': base64.encodestring(file.read()),
                'datas_fname': file.filename,
                'res_model': self._name,
                'res_id': record.id,
                'description': question.question,
            }
            vals.update({'answer_type': 'binary'})
            if attachment_value and not record.attachment_id:
                # Create New Attachment if not Found
                attachment = attachment.create(attachment_value)
                vals.update({'attachment_id': attachment.id})
        else:
            vals.update({'answer_type': None, 'skipped': True})

        if record:
            # Write Record
            # print('save_line_binary - attachment_value для врайта>>>>>>>>', attachment_value)
            if attachment_value and record.attachment_id:
                record.attachment_id.sudo().write(attachment_value)
            record.sudo().write(vals)
        else:
            # Create New Record
            # Link Created Attachment
            record = self.sudo(SUPERUSER_ID).create(vals)
            # print('save_line_binary - новый рекорд>>>>>>>>', record)
            if attachment:
                attachment.write({'res_id': record.id})
                main_answer = self.env['survey.user_input'].sudo(
                    SUPERUSER_ID).browse(user_input_id)
                main_answer.attachment_ids = [(4, attachment.id)]
                # print('save_line_binary - main_answer>>>>>>>>', main_answer, '\n')
        return True
