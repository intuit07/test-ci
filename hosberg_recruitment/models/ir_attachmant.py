# -*- coding: utf-8 -*-

##############################################################################
#
#    Clear Groups for Odoo
#    Copyright (C) 2016 Bytebrand GmbH (<http://www.bytebrand.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
import hashlib
import itertools
import logging

from odoo import api, fields, models, tools, SUPERUSER_ID, _


_logger = logging.getLogger(__name__)


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'

    to_delete = fields.Boolean('To Delete')

    @api.model
    def create(self, vals):
        if vals.get('name') and vals.get('name') in ['Lebenslauf_mit_Foto',
                                                     'Arbeitszeugnisse',
                                                     'Zertifikate_Diplome',
                                                     'Ausbildungszeugnisse',
                                                     'Weitere_Anlagen_1',
                                                     'Weitere_Anlagen_2']:
            vals['to_delete'] = True
        return super(IrAttachment, self).create(vals)

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        result = super(IrAttachment, self).read(fields=fields, load=load)
        if self.env.context.get('active_model') == 'hr.applicant':
            if self.env.uid == 1 or self.env.user.has_group(
                    'hr_recruitment.group_hr_recruitment_manager'):
                return result
            elif self.env.user.has_group(
                    'hr_recruitment.group_hr_recruitment_user'):
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', self.env.uid)], limit=1)
                if employee:
                    departments_ids = self.env['hr.department'].search([
                        ('manager_id', '=', employee.id)]).ids
                    if employee.department_id.id not in departments_ids:
                        departments_ids += [employee.department_id.id, ]
                    child_department_ids = self.find_department(departments_ids)
                    if child_department_ids:
                        for child_department_id in child_department_ids:
                            if child_department_id not in departments_ids:
                                departments_ids += child_department_ids
                    applicant_ids = self.env['hr.applicant'].search(
                        [('department_id', 'in', departments_ids)]).ids
                    res = []
                    for r in result:
                        if self.env['ir.attachment'].search(
                                [('id', '=', r.get('id'))]
                        ).res_id in applicant_ids:
                            res.append(r)
                    return res
        return result

    @api.multi
    def find_department(self, department_ids):
        for department_id in department_ids:
            child_department = self.env['hr.department'].search(
                [('parent_id', '=', department_id)]).ids
            department_ids += child_department
            if child_department:
                department_ids += self.find_department(child_department)
        return department_ids
