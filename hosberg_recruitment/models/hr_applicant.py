# -*- coding: utf-8 -*-

##############################################################################
#
#    Clear Groups for Odoo
#    Copyright (C) 2016 Bytebrand GmbH (<http://www.bytebrand.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
import logging
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

AVAILABLE_PRIORITIES = [
    ('0', 'Normal'),
    ('1', 'Absagen'),
    ('2', 'Vertrösten'),
    ('3', 'Einladen')
]


class Applicant(models.Model):
    _inherit = 'hr.applicant'

    job_room = fields.Char(string="Job-Room Stellen-Nr.")

    title_id = fields.Many2one('res.partner.title', string='Salutation')
    first_name = fields.Char(string="First name")
    last_name = fields.Char(string="Last Name")
    street = fields.Char(string="Street")
    zip = fields.Char(string="Zip")
    city = fields.Char(string="Zip")
    birthday = fields.Char(string='Birthday')
    residence_permit = fields.Selection([
        ('0', ''),
        ('b_ausweis', 'B-Ausweis EU/EFTA (Aufenthaltsbewilligung)'),
        ('b_biometrischer',
         'B-Biometrischer Ausländerausweis Nicht-EU/EFTA (Aufenthaltsbewälligung)'),
        ('c_ausweis', 'C-Ausweis EU/EFTA (Niederlassungsbewilligung)'),
        ('c_biometrischer',
         'C-Biometrischer Auslängerausweis Nicht-EU/EFTA (Niederlassungsbewilligung)'),
        ('ci_ausweis',
         'Ci-Ausweis EU/EFTA (Aufenthaltsbewilligung mit Erwerbstätigkeit)'),
        ('ci_ausweis_nicht',
         'Ci-Ausweis Nicht-EU/EFTA (Aufenthaltsbewilligung mit Erwerbstätigkeit)'),
        ('g_ausweis', 'G-Ausweis EU/EFTA (Grenzgängerbewilligung)'),
        ('g_ausweis_nicht', 'G-Ausweis Nicht-EW/EFTA (Grenzgängerbewilligung)'),
        ('l_ausweis', 'L-Ausweis EU/EFTA (Kurzaufenthaltsbewäligung)'),
        ('l_biometrischer',
         'L-Biometrischer Ausweis Nicht-EU/EFTA (Kurzaufenthaltsbewilligung)'),
        ('f_ausweis_nicht',
         'F-Ausweis Nicht EU/EFTA (Vorläufig aufgenommene Ausläner)'),
        ('n_ausweis_nicht', 'N-Ausweis Nicht EU/EFTA (für Asylsuchende)'),
        ('s_ausweis_nicht', 'S-Ausweis Nicht-EW/EFTA (für Schutzbedürftige)')],
        string="Residence Permit",
        default="0")

    residence_permit_to = fields.Char(string='Residence permit valid until')
    earliest_entry = fields.Char(string='Earliest Entry')

    survey_ids = fields.One2many('applicant.surveys',
                                 'applicant_id',
                                 string='Surveys')
    interview_number = fields.Char(string='Interview Number',
                                   compute='compute_interview_number')
    therm_conditions = fields.Boolean()
    click = fields.Boolean(default=False)
    priority_hr = fields.Selection(AVAILABLE_PRIORITIES, "Bewertung HR", default='0')
    priority_al = fields.Selection(AVAILABLE_PRIORITIES, "Bewertung AL", default='0')
    for_custom_email = fields.Boolean(default=False)

    country_id = fields.Many2one('res.country', 'Country')
    state_id = fields.Many2one(
        'res.country.state', string='Country State',
        domain="[('country_id', '=', country_id)]")
    stage_for_creation_empl = fields.Boolean(
        string='Stage for Creation Employee',
        related='stage_id.stage_for_creation_empl')

    def website_form_input_filter(self, request, values):
        if 'partner_name' in values:
            a = _('Application')
            values.setdefault('name', '%s\'s %s' % values['partner_name'], a)
        return values

    @api.multi
    def compute_interview_number(self):
        for applicant in self:
            applicant.interview_number = '{} / {}'.format(
                self.env['applicant.surveys'].search_count(
                    [('applicant_id', '=', applicant.id),
                     ('state', '=', 'done')]),
                self.env['applicant.surveys'].search_count(
                    [('applicant_id', '=', applicant.id)])
            )

    @api.multi
    def write(self, values):
        if values.get('job_id'):
            job = self.env['hr.job'].browse(values.get('job_id'))
            if self.survey_ids:
                job_survey_ids = [s.id for s in job.survey_ids]
                for survey_id in self.survey_ids:
                    if survey_id.survey_id.id not in job_survey_ids:
                        survey_id.unlink()

                self_survey_ids = [s.survey_id.id for s in self.survey_ids]
                for survey_id in job.survey_ids:
                    if survey_id.id not in self_survey_ids:
                        self.env['applicant.surveys'].create(
                            {'applicant_id': self.id,
                             'survey_id': survey_id.id})

            else:
                for survey in job.survey_ids:
                    self.env['applicant.surveys'].create(
                        {'applicant_id': self.id,
                         'survey_id': survey.id})
        if values.get('stage_id'):
            stage = self.env['hr.recruitment.stage'].browse(
                values.get('stage_id'))
            if stage.to_delete:
                for att in self.attachment_ids:
                    if att.to_delete:
                        att.unlink()
        return super(Applicant, self).write(values)

    @api.multi
    def create_employee_from_applicant(self):
        """ Create an hr.employee from the hr.applicants """
        employee = False
        for applicant in self:
            contact_name = False
            if applicant.partner_id:
                address_id = applicant.partner_id.address_get(['contact'])[
                    'contact']
                contact_name = applicant.partner_id.name_get()[0][1]
            else:
                new_partner_id = self.env['res.partner'].create({
                    'is_company': False,
                    'name': applicant.partner_name,
                    'email': applicant.email_from,
                    'phone': applicant.partner_phone,
                    'mobile': applicant.partner_mobile
                })
                address_id = new_partner_id.address_get(['contact'])['contact']
            if applicant.job_id and (applicant.partner_name or contact_name):
                applicant.job_id.write(
                    {'no_of_hired_employee':
                         applicant.job_id.no_of_hired_employee + 1})
                employee = self.env['hr.employee'].create({
                    'name': applicant.partner_name or contact_name,
                    'job_id': applicant.job_id.id,
                    'address_home_id': address_id,
                    'department_id': applicant.department_id.id or False,
                    'address_id': applicant.company_id
                                  and applicant.company_id.partner_id
                                  and applicant.company_id.partner_id.id
                                  or False,
                    'work_email': applicant.department_id
                                  and applicant.department_id.company_id
                                  and applicant.department_id.company_id.email
                                  or False,
                    'work_phone': applicant.department_id
                                  and applicant.department_id.company_id
                                  and applicant.department_id.company_id.phone
                                  or False,
                    'country_id':  applicant.country_id.id,
                    'state_id':  applicant.state_id.id, })
                applicant.write({'emp_id': employee.id})
                applicant.job_id.message_post(
                    body=_('New Employee %s Hired') % applicant.partner_name
                    if applicant.partner_name else applicant.name,
                    subtype="hr_recruitment.mt_job_applicant_hired")
                employee._broadcast_welcome()
            else:
                raise UserError(_(
                    'You must define an Applied Job and a '
                    'Contact Name for this applicant.'))

        employee_action = self.env.ref('hr.open_view_employee_list')
        dict_act_window = employee_action.read([])[0]
        if employee:
            dict_act_window['res_id'] = employee.id
        dict_act_window['view_mode'] = 'form,tree'
        return dict_act_window

    @api.model
    def create(self, values):
        if values.get('first_name') or values.get('last_name'):
            values['partner_name'] = '{} {}'.format(
                values.get('first_name') or '',
                values.get('last_name') or '', )
        # change the date for the standard format
        if values.get('birthday'):
            list_date = values.get('birthday').split('-')
            values['birthday'] = '{}-{}-{}'.format(list_date[2],
                                                   list_date[1],
                                                   list_date[0])
        if values.get('earliest_entry'):
            list_date = values.get('earliest_entry').split('-')
            values['earliest_entry'] = '{}-{}-{}'.format(list_date[2],
                                                         list_date[1],
                                                         list_date[0])
        if values.get('residence_permit_to'):
            list_date = values.get('residence_permit_to').split('-')
            values['residence_permit_to'] = '{}-{}-{}'.format(list_date[2],
                                                              list_date[1],
                                                              list_date[0])
        # change name for the custom format/language
        if values:
            values['name'] = '%s\'s Bewerbung' % values['partner_name']
        elif self._context.get('lang') == 'en_US':
            values['name'] = '%s\'s Application' % values['partner_name']
        values['for_custom_email'] = True
        applicant = super(Applicant, self).create(values)
        if not applicant.partner_id and applicant.email_from:
            val = {'name': applicant.partner_name or applicant.email_from,
                   'email': applicant.email_from}
            partner = self.env['res.partner'].create(val)
            applicant.message_follower_ids.unlink()
            self.env['mail.followers'].create({'res_model': 'hr.applicant',
                                               'res_id': applicant.id,
                                               'partner_id': partner.id})
            applicant.write({'partner_id': partner.id})

        if applicant.job_id:
            for survey in applicant.job_id.survey_ids:
                self.env['applicant.surveys'].create(
                    {'applicant_id': applicant.id,
                     'survey_id': survey.id})
        model = self.env['ir.model'].search([('model', '=', 'hr.applicant')])
        template_id = self.env['mail.template'].search(
            [('model_id', '=', model.id),
             ('default_template', '=', True)], limit=1)
        if template_id:
            applicant.message_post_with_template(template_id.id)
        return applicant

    @api.multi
    def open_message_wizard(self):
        ir_model_data = self.env['ir.model.data']
        try:
            compose_form_id = ir_model_data.get_object_reference(
                'mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False

        partner_ids = [follower.partner_id.id for follower in
                       self.message_follower_ids]
        if self.partner_id:
            partner_ids += [self.partner_id.id, ]
        ctx = {
            'default_model': 'hr.applicant',
            'default_res_id': self.ids[0],
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'force_email': True,
            'default_partner_ids': [(6, 0, partner_ids)],
        }
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_start_surveys(self):
        """ Open the website page with the survey form """
        ir_model_data = self.env['ir.model.data']
        try:
            compose_form_id = ir_model_data.get_object_reference(
                'survey', 'survey_tree')[1]
        except ValueError:
            compose_form_id = False

        partner_ids = [follower.partner_id.id for follower in
                       self.message_follower_ids]
        if self.partner_id:
            partner_ids += [self.partner_id.id, ]
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'applicant.surveys',
            'domain': [('applicant_id', '=', self.id)],
            # 'views': [(compose_form_id, 'tree')],
            # 'view_id': compose_form_id,
            'target': 'current',
            # 'context': ctx,
        }

    @api.multi
    def find_department(self, department_ids):
        for department_id in department_ids:
            child_department = self.env['hr.department'].search(
                [('parent_id', '=', department_id)]).ids
            department_ids += child_department
            if child_department:
                department_ids += self.find_department(child_department)
        return department_ids

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        res = super(Applicant, self).read(fields=fields, load=load)
        if self.env.uid == 1 or self.env.user.has_group(
                'hr_recruitment.group_hr_recruitment_manager'):
            return res
        elif self.env.user.has_group(
                'hr_recruitment.group_hr_recruitment_user'):
            employee = self.env['hr.employee'].search(
                [('user_id', '=', self.env.uid)], limit=1)
            if employee:
                result = []
                departments_ids = self.env['hr.department'].search([
                    ('manager_id', '=', employee.id)]).ids
                if employee.department_id.id not in departments_ids:
                    departments_ids += [employee.department_id.id, ]
                child_department_ids = self.find_department(departments_ids)
                if child_department_ids:
                    for child_department_id in child_department_ids:
                        if child_department_id not in departments_ids:
                            departments_ids += child_department_ids
                for r in res:
                    if r.get('department_id'):
                        if type(r.get('department_id')) == int \
                                and r.get('department_id') in departments_ids:
                            result.append(r)
                        elif r.get('department_id')[0] in departments_ids:
                            result.append(r)
                return result
        return []

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None,
                   orderby=False, lazy=True):
        if self.env.uid == 1 or self.env.user.has_group(
                'hr_recruitment.group_hr_recruitment_manager'):
            return super(Applicant, self).read_group(domain, fields, groupby,
                                                    offset=offset, limit=limit,
                                                    orderby=orderby, lazy=lazy)
        elif self.env.user.has_group(
                'hr_recruitment.group_hr_recruitment_user'):
            employee = self.env['hr.employee'].search(
                [('user_id', '=', self.env.uid)], limit=1)
            res = []
            if employee:

                departments_ids = self.env['hr.department'].search([
                    ('manager_id', '=', employee.id)]).ids
                if employee.department_id.id not in departments_ids:
                    departments_ids += [employee.department_id.id, ]
                child_department_ids = self.find_department(departments_ids)
                if child_department_ids:
                    for child_department_id in child_department_ids:
                        if child_department_id not in departments_ids:
                            departments_ids += child_department_ids
                domain += [('department_id', 'in', departments_ids) ]
                res = super(Applicant, self).read_group(
                    domain, fields, groupby, offset=offset, limit=limit,
                    orderby=orderby, lazy=lazy)
            return res

    @api.multi
    def _message_auto_subscribe_notify(self, partner_ids):
        if self.for_custom_email == True:
            self.for_custom_email = False
            template_responsib = self.env['ir.model.data'].get_object_reference(
                'hosberg_recruitment',
                'email_template_responsib_hr')[1]
            responsib_template = self.env['mail.template'].browse(
                template_responsib)
            responsib_template.send_mail(res_id=self.id, force_send=False)
        else:
            return super(Applicant, self)._message_auto_subscribe_notify(
                partner_ids)
