# -*- coding: utf-8 -*-

##############################################################################
#
#    Clear Groups for Odoo
#    Copyright (C) 2016 Bytebrand GmbH (<http://www.bytebrand.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)


class Job(models.Model):
    _inherit = "hr.job"

    survey_ids = fields.Many2many(
        'survey.survey',
        'rel_survey_job',
        'job_id',
        'survey_id',
        string="Interview Forms",
        help="Choose an interview form for this job position and you will "
             "be able to print/answer this interview from all "
             "applicants who apply for this job",
        domain=[('recruitment_survey', '=', True)])
    agb = fields.Many2one('ir.attachment', string="AGB")
    for_custom_email = fields.Boolean(default=False)
    hr_responsible_id = fields.Many2one('res.users', "HR Responsible",
                                        track_visibility='onchange',
                                        help="Person responsible of validating the employee's contracts.",
                                        required=True)
    user_id = fields.Many2one('res.users', required=True)

    @api.multi
    def find_department(self, department_ids):
        for department_id in department_ids:
            child_department = self.env['hr.department'].search(
                [('parent_id', '=', department_id)]).ids
            department_ids += child_department
            if child_department:
                department_ids += self.find_department(child_department)
        return department_ids

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        job = self.sudo()
        res = super(Job, self).read(fields=fields, load=load)
        if self.env.uid == 1 or self.env.user.has_group(
                'hr_recruitment.group_hr_recruitment_manager'):
            return res
        elif self.env.user.has_group(
                'hr_recruitment.group_hr_recruitment_user'):
            employee = self.env['hr.employee'].search(
                [('user_id', '=', self.env.uid)], limit=1)
            if employee:
                result = []
                departments_ids = self.env['hr.department'].search(
                    [('manager_id', '=', employee.id)]).ids
                if employee.department_id.id not in departments_ids:
                    departments_ids += [employee.department_id.id, ]
                child_department_ids = self.find_department(departments_ids)
                for child_department_id in child_department_ids:
                    if child_department_id not in departments_ids:
                        departments_ids += [child_department_ids, ]
                for r in res:
                    if r.get('department_id'):
                        if type(r.get('department_id')) == int:
                            if r.get('department_id') in departments_ids:
                                result.append(r)
                        else:
                            if r.get('department_id')[0] in departments_ids:
                                result.append(r)
                return result
        return []

    @api.model
    def create(self, vals):
        vals['for_custom_email'] = True
        return super(Job, self).create(vals)

    @api.multi
    def _message_auto_subscribe_notify(self, partner_ids):
        if self.for_custom_email is True and self.hr_responsible_id:
            template_responsib = self.env['ir.model.data'].get_object_reference(
                'hosberg_recruitment',
                'email_template_responsib_hr_job')[1]
            responsib_template = self.env['mail.template'].browse(template_responsib)
            responsib_template.send_mail(res_id=self.id, force_send=False)
            if self.user_id:
                template = self.env['ir.model.data'].get_object_reference(
                    'hosberg_recruitment',
                    'email_template_responsib')[1]
                recruiter_responsib_template = self.env['mail.template'].browse(template)
                recruiter_responsib_template.send_mail(res_id=self.id, force_send=False)
            self.for_custom_email = False
        else:
            return super(Job, self)._message_auto_subscribe_notify(partner_ids)

