# -*- coding: utf-8 -*-

##############################################################################
#
#    Clear Groups for Odoo
#    Copyright (C) 2016 Bytebrand GmbH (<http://www.bytebrand.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)


class Message(models.Model):
    _inherit = 'mail.message'

    @api.multi
    def _notify(self, force_send=False, send_after_commit=True,
                user_signature=True):
        if not self.env.context.get('active_model') == 'hr.applicant':
            return super(Message, self)._notify(
                force_send=force_send, send_after_commit=send_after_commit,
                user_signature=user_signature)
        else:
            group_user = self.env.ref('base.group_user')
            self_sudo = self.sudo()

            self.ensure_one()
            partners_sudo = self_sudo.partner_ids
            channels_sudo = self_sudo.channel_ids

            # if self_sudo.subtype_id and self.model and self.res_id:
            #     followers = self_sudo.env['mail.followers'].search([
            #         ('res_model', '=', self.model),
            #         ('res_id', '=', self.res_id),
            #         ('subtype_ids', 'in', self_sudo.subtype_id.id),
            #     ])
            #     if self_sudo.subtype_id.internal:
            #         followers = followers.filtered(
            #             lambda fol: fol.channel_id or (
            #                 fol.partner_id.user_ids and group_user in
            #                 fol.partner_id.user_ids[0].mapped('groups_id')))
            #     channels_sudo |= followers.mapped('channel_id')
            #     partners_sudo |= followers.mapped('partner_id')

            if not self._context.get('mail_notify_author', False) \
                    and self_sudo.author_id:
                partners_sudo = partners_sudo - self_sudo.author_id

            # update message, with maybe custom values
            message_values = {}
            if channels_sudo:
                message_values['channel_ids'] = [(6, 0, channels_sudo.ids)]
            if partners_sudo:
                message_values['needaction_partner_ids'] = [
                    (6, 0, partners_sudo.ids)]
            if self.model and self.res_id and hasattr(
                    self.env[self.model], 'message_get_message_notify_values'):
                message_values.update(self.env[self.model].browse(
                    self.res_id).message_get_message_notify_values(
                    self, message_values))
            if message_values:
                self.write(message_values)

            email_channels = channels_sudo.filtered(
                lambda channel: channel.email_send)
            notif_partners = partners_sudo.filtered(
                lambda partner: 'inbox' in partner.mapped(
                    'user_ids.notification_type'))
            if email_channels or partners_sudo - notif_partners:
                partners_sudo.search([
                    '|',
                    ('id', 'in', (partners_sudo - notif_partners).ids),
                    ('channel_ids', 'in', email_channels.ids),
                    ('email', '!=',
                     self_sudo.author_id.email or self_sudo.email_from),
                ])._notify(self, force_send=force_send,
                           send_after_commit=send_after_commit,
                           user_signature=user_signature)

            notif_partners._notify_by_chat(self)
            channels_sudo._notify(self)
            if self.parent_id:
                self.parent_id.invalidate_cache()
            return True
